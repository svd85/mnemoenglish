frontend-clear:
	docker run --rm -v ${PWD}/frontend:/app -w /app alpine sh -c 'rm -rf .ready build'

frontend-init: frontend-yarn-install frontend-ready

frontend-yarn-install:
	docker-compose run --rm frontend-node-cli yarn install

frontend-ready:
	docker run --rm -v ${PWD}/frontend:/app -w /app alpine touch .ready

build-frontend:
	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull --build-arg BUILDKIT_INLINE_CACHE=1 \
	--target builder \
	--cache-from ${REGISTRY}/mnemoenglish-frontend:cache-builder \
	--tag ${REGISTRY}/mnemoenglish-frontend:cache-builder \
	--file frontend/docker/prod/nginx/Dockerfile frontend

	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull --build-arg BUILDKIT_INLINE_CACHE=1 \
	--cache-from ${REGISTRY}/mnemoenglish-frontend:cache-builder \
	--cache-from ${REGISTRY}/mnemoenglish-frontend:cache \
	--tag ${REGISTRY}/mnemoenglish-frontend:cache \
	--tag ${REGISTRY}/mnemoenglish-frontend:${IMAGE_TAG} \
	--file frontend/docker/prod/nginx/Dockerfile frontend

push-cache-frontend:
	docker push ${REGISTRY}/mnemoenglish-frontend:cache-builder
	docker push ${REGISTRY}/mnemoenglish-frontend:cache

push-frontend:
	docker push ${REGISTRY}/mnemoenglish-frontend:${IMAGE_TAG}