init: docker-down-clear \
	docker-pull docker-build docker-up \
 	api-init

up: docker-up
down: docker-down
restart: down up
lint: api-lint
test: api-test

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	- docker-compose pull --include-deps

docker-build:
	DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose build --build-arg BUILDKIT_INLINE_CACHE=1

push-dev-cache:
	docker-compose push

api-clear:
	docker run --rm -v ${PWD}/api:/app -w /app alpine truncate -s 0 /app/storage/logs/laravel.log

api-lint:
	docker-compose run --rm api-php-cli composer lint
	docker-compose run --rm api-php-cli composer cs-check

api-test:
	docker-compose run --rm api-php-cli composer test

api-init: api-permissions api-composer-install api-wait-db api-migrations api-fixtures

api-permissions:
	docker run --rm -v ${PWD}/api:/app -w /app alpine chmod -R 777 storage

api-composer-install:
	docker-compose run --rm api-php-cli composer install

api-wait-db:
	docker-compose run --rm api-php-cli wait-for-it api-postgres:5432 -t 30

api-migrations:
	docker-compose run --rm api-php-cli php artisan migrate --force
	docker-compose run --rm api-php-cli php artisan module:migrate --force

api-fixtures:
	docker-compose run --rm api-php-cli php artisan db:seed --force

build: build-gateway build-api

build-gateway:
	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull --build-arg BUILDKIT_INLINE_CACHE=1 \
	--cache-from ${REGISTRY}/mnemoenglish-gateway:cache \
	--tag ${REGISTRY}/mnemoenglish-gateway:cache \
	--tag ${REGISTRY}/mnemoenglish-gateway:${IMAGE_TAG} \
	--file gateway/docker/prod/nginx/Dockerfile gateway/docker

build-api: build-api-nginx build-api-fpm build-api-cli

build-api-nginx:
	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull \
	--build-arg BUILDKIT_INLINE_CACHE=1 \
	--cache-from ${REGISTRY}/mnemoenglish-api:cache \
	--tag ${REGISTRY}/mnemoenglish-api:cache \
	--tag ${REGISTRY}/mnemoenglish-api:${IMAGE_TAG} \
	--file api/docker/prod/nginx/Dockerfile api

build-api-cli:
	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull \
	--build-arg BUILDKIT_INLINE_CACHE=1 \
	--target builder \
	--cache-from ${REGISTRY}/mnemoenglish-api-php-cli:cache-builder \
	--tag ${REGISTRY}/mnemoenglish-api-php-cli:cache-builder \
	--file api/docker/prod/php-cli/Dockerfile api

	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull \
	--build-arg BUILDKIT_INLINE_CACHE=1 \
	--cache-from ${REGISTRY}/mnemoenglish-api-php-cli:cache-builder \
	--cache-from ${REGISTRY}/mnemoenglish-api-php-cli:cache \
	--tag ${REGISTRY}/mnemoenglish-api-php-cli:cache \
	--tag ${REGISTRY}/mnemoenglish-api-php-cli:${IMAGE_TAG} \
	--file api/docker/prod/php-cli/Dockerfile api

build-api-fpm:
	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull \
	--build-arg BUILDKIT_INLINE_CACHE=1 \
	--target builder \
	--cache-from ${REGISTRY}/mnemoenglish-api-php-fpm:cache-builder \
	--tag ${REGISTRY}/mnemoenglish-api-php-fpm:cache-builder \
	--file api/docker/prod/php-fpm/Dockerfile api

	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull \
	--build-arg BUILDKIT_INLINE_CACHE=1 \
	--cache-from ${REGISTRY}/mnemoenglish-api-php-fpm:cache-builder \
	--cache-from ${REGISTRY}/mnemoenglish-api-php-fpm:cache \
	--tag ${REGISTRY}/mnemoenglish-api-php-fpm:cache \
	--tag ${REGISTRY}/mnemoenglish-api-php-fpm:${IMAGE_TAG} \
	--file api/docker/prod/php-fpm/Dockerfile api

try-build:
	REGISTRY=localhost IMAGE_TAG=0 make build

build-cache: push-cache-gateway push-cache-api
push: push-gateway push-api

push-cache-gateway:
	docker push ${REGISTRY}/mnemoenglish-gateway:cache
push-gateway:
	docker push ${REGISTRY}/mnemoenglish-gateway:${IMAGE_TAG}

push-cache-api:
	docker push ${REGISTRY}/mnemoenglish-api:cache
	docker push ${REGISTRY}/mnemoenglish-api-php-fpm:cache-builder
	docker push ${REGISTRY}/mnemoenglish-api-php-fpm:cache
	docker push ${REGISTRY}/mnemoenglish-api-php-cli:cache-builder
	docker push ${REGISTRY}/mnemoenglish-api-php-cli:cache

push-api:
	docker push ${REGISTRY}/mnemoenglish-api:${IMAGE_TAG}
	docker push ${REGISTRY}/mnemoenglish-api-php-fpm:${IMAGE_TAG}
	docker push ${REGISTRY}/mnemoenglish-api-php-cli:${IMAGE_TAG}

deploy:
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'rm -rf site_${BUILD_NUMBER}'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'mkdir site_${BUILD_NUMBER}'
	scp -o StrictHostKeyChecking=no -P ${PORT} docker-compose-production.yml deploy@${HOST}:site_${BUILD_NUMBER}/docker-compose.yml
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "COMPOSE_PROJECT_NAME=mnemoEnglish" >> .env'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "REGISTRY=${REGISTRY}" >> .env'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "IMAGE_TAG=${IMAGE_TAG}" >> .env'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "APP_ENV=production" >> .env'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "APP_DEBUG=true" >> .env'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "LOG_LEVEL=debug" >> .env'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "APP_KEY=base64:eQz5g79atMFnBJQ+kV7fkYbAkX4LKzd3rFJOGQj/xpA=" >> .env'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "APP_URL=https://api.mnemo-english.ru" >> .env'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "DB_CONNECTION=pgsql" >> .env'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "DB_HOST=api-postgres" >> .env'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "DB_PORT=5432" >> .env'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "DB_DATABASE=app" >> .env'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "DB_USERNAME=app" >> .env'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "DB_PASSWORD=secret" >> .env'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && docker-compose pull'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && docker-compose up --build -d api-postgres api-php-cli'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && docker-compose run api-php-cli wait-for-it api-postgres:5432 -t 60'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && docker-compose run api-php-cli php artisan migrate --no-interaction --force'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && docker-compose run api-php-cli php artisan module:migrate --no-interaction --force'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && docker-compose up --build --remove-orphans -d'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'rm -f site'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'ln -sr site_${BUILD_NUMBER} site'

rollback:
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && docker-compose pull'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && docker-compose up --build --remove-orphans -d'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'rm -f site'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${PORT} 'ln -sr site_${BUILD_NUMBER} site'

#тестирование на дубли в коде
phpcpd:
	wget "https://phar.phpunit.de/phpcpd.phar" && php phpcpd.phar --fuzzy --exclude=vendor --exclude=nova --exclude=database .
